#include <stdlib.h>
#include "platform.h"
#include "xil_mmu.h"
#include "xil_cache.h"
#include "xil_cache_l.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <assert.h>
#include "squeezenet.h"

#define NUM 1

//declare names for programming registers

void print(char *str);

//Convolution layer 1 registers
volatile char *control = (volatile char*)0x43C00000;
volatile int *wg_x = (volatile int*)0x43C00010;
volatile int *wg_y = (volatile int*)0x43C00018;
volatile int *wg_z = (volatile int*)0x43C00020;
volatile int *Layer1_weights_HW = (volatile int*)0x43C00028;
volatile int *data_R = (volatile int*)0x43C00030;
volatile int *data_G = (volatile int*)0x43C00038;
volatile int *data_B = (volatile int*)0x43C00040;
volatile int *Layer1_Features_HW = (volatile int*)0x43C00048;

//Pooling layer 1 registers
volatile char *control_p = (volatile char*)0x43C10000;
volatile int *wg_x_p = (volatile int*)0x43C10010;
volatile int *wg_y_p = (volatile int*)0x43C10018;
volatile int *wg_z_p = (volatile int*)0x43C10020;
volatile int *Layer1_Features_HW_p = (volatile int*)0x43C10028;
volatile int *Layer1_Pool_HW_p = (volatile int*)0x43C10030;

//Fire 2 layer registers
volatile char *control_f2 = (volatile char*)0x43C20000;
volatile int *wg_x_f2 = (volatile int*)0x43C20010;
volatile int *wg_y_f2 = (volatile int*)0x43C20018;
volatile int *wg_z_f2 = (volatile int*)0x43C20020;
volatile int *Layer2_pool_GPU_HW = (volatile int*)0x43C20028;
volatile int *fire2squeeze1x1_Weights_HW = (volatile int*)0x43C20030;
volatile int *fire2squeeze1x1_Features_HW = (volatile int*)0x43C20038;
volatile int *fire2expand1x1_Weights_HW = (volatile int*)0x43C20040;
volatile int *fire2expand3x3_Weights_HW = (volatile int*)0x43C20048;
volatile int *fire2_Features_HW = (volatile int*)0x43C20050;

////Fire 3 squeeze layer registers
volatile char *control_f3_s = (volatile char*)0x43C30000;
volatile int *wg_x_f3_s = (volatile int*)0x43C30010;
volatile int *wg_y_f3_s = (volatile int*)0x43C30018;
volatile int *wg_z_f3_s = (volatile int*)0x43C30020;
volatile int *fire2_Features_HW_new = (volatile int*)0x43C30028;
volatile int *fire3squeeze1x1_Weights_HW = (volatile int*)0x43C30030;
volatile int *fire3squeeze1x1_Features_HW = (volatile int*)0x43C30038;

//Fire 3 expand layer registers
volatile char *control_f3_e = (volatile char*)0x43C40000;
volatile int *wg_x_f3_e = (volatile int*)0x43C40010;
volatile int *wg_y_f3_e = (volatile int*)0x43C40018;
volatile int *wg_z_f3_e = (volatile int*)0x43C40020;
volatile int *fire3_squeeze_HW = (volatile int*)0x43C40028;
volatile int *fire3expand1x1_Weights_HW= (volatile int*)0x43C40030;
volatile int *fire3expand3x3_Weights_HW= (volatile int*)0x43C40038;
volatile int *fire3_Features_HW = (volatile int*)0x43C40040;


//Fire 4 layer registers
volatile char *control_f4 = (volatile char*)0x43C50000;
volatile int *wg_x_f4 = (volatile int*)0x43C50010;
volatile int *wg_y_f4 = (volatile int*)0x43C50018;
volatile int *wg_z_f4 = (volatile int*)0x43C50020;
volatile int *fire3_Features_HW_new = (volatile int*)0x43C50028;
volatile int *fire4squeeze1x1_Weights_HW = (volatile int*)0x43C50030;
volatile int *fire4squeeze1x1_Features_HW = (volatile int*)0x43C50038;
volatile int *fire4expand1x1_Weights_HW= (volatile int*)0x43C50040;
volatile int *fire4expand3x3_Weights_HW= (volatile int*)0x43C50048;
volatile int *fire4_Features_HW = (volatile int*)0x43C50050;


//Pooling layer 4 registers
volatile char *control_f4_p4 = (volatile char*)0x43C60000;
volatile int *wg_x_f4_p4 = (volatile int*)0x43C60010;
volatile int *wg_y_f4_p4 = (volatile int*)0x43C60018;
volatile int *wg_z_f4_p4 = (volatile int*)0x43C60020;
volatile int *fire4_Features_HW_new = (volatile int*)0x43C60028;
volatile int *Layer4_Pool_HW_p = (volatile int*)0x43C60030;

//Fire 5 layer registers
volatile char *control_f5 = (volatile char*)0x43C70000;
volatile int *wg_x_f5 = (volatile int*)0x43C70010;
volatile int *wg_y_f5 = (volatile int*)0x43C70018;
volatile int *wg_z_f5 = (volatile int*)0x43C70020;
volatile int *Layer4_Pool_HW_p_new = (volatile int*)0x43C70028;
volatile int *fire5squeeze1x1_Weights_HW = (volatile int*)0x43C70030;
volatile int *fire5squeeze1x1_Features_HW = (volatile int*)0x43C70038;
volatile int *fire5expand1x1_Weights_HW= (volatile int*)0x43C70040;
volatile int *fire5expand3x3_Weights_HW= (volatile int*)0x43C70048;
volatile int *fire5_Features_HW = (volatile int*)0x43C70050;

//Fire 6 layer registers
volatile char *control_f6 = (volatile char*)0x43C80000;
volatile int *wg_x_f6 = (volatile int*)0x43C80010;
volatile int *wg_y_f6 = (volatile int*)0x43C80018;
volatile int *wg_z_f6 = (volatile int*)0x43C80020;
volatile int *fire5_Features_HW_new = (volatile int*)0x43C80028;
volatile int *fire6squeeze1x1_Weights_HW = (volatile int*)0x43C80030;
volatile int *fire6squeeze1x1_Features_HW = (volatile int*)0x43C80038;
volatile int *fire6expand1x1_Weights_HW= (volatile int*)0x43C80040;
volatile int *fire6expand3x3_Weights_HW= (volatile int*)0x43C80048;
volatile int *fire6_Features_HW = (volatile int*)0x43C80050;


//Fire 7 layer registers
volatile char *control_7 = (volatile char*)0x43C90000;
volatile int *wg_x_7 = (volatile int*)0x43C90010;
volatile int *wg_y_7 = (volatile int*)0x43C90018;
volatile int *wg_z_7 = (volatile int*)0x43C90020;
volatile int *fire7squeeze1x1_Weights_hw = (volatile int*)0x43C90028;
volatile int *fire7expand1x1_Weights_hw = (volatile int*)0x43C90030;
volatile int *fire7expand3x3_Weights_hw = (volatile int*)0x43C90038;
volatile int *Features_7sq1x1_hw = (volatile int*)0x43C90040;
volatile int *fire6_Features_hw = (volatile int*)0x43C90048;
volatile int *fire7_Features_hw = (volatile int*)0x43C90050;

//Fire 8 squeeze layer registers
volatile char *control_8s = (volatile char*)0x43CA0000;
volatile int *wg_x_8s = (volatile int*)0x43CA0010;
volatile int *wg_y_8s = (volatile int*)0x43CA0018;
volatile int *wg_z_8s = (volatile int*)0x43CA0020;
volatile int *fire8squeeze1x1_Weights_hw = (volatile int*)0x43CA0028;
volatile int *fire7_Features_hw_8s = (volatile int*)0x43CA0040;
volatile int *Features_8sq1x1_hw = (volatile int*)0x43CA0048;

//Fire 8 expand layer registers
volatile char *control_8e = (volatile char*)0x43CB0000;
volatile int *wg_x_8e = (volatile int*)0x43CB0010;
volatile int *wg_y_8e = (volatile int*)0x43CB0018;
volatile int *wg_z_8e = (volatile int*)0x43CB0020;
volatile int *fire8expand1x1_Weights_hw = (volatile int*)0x43CB0028;
volatile int *fire8expand3x3_Weights_hw = (volatile int*)0x43CB0030;
volatile int *Features_8sq1x1_hw_8e = (volatile int*)0x43CB0038;
volatile int *fire8_Features_hw = (volatile int*)0x43CB0040;
volatile int *layer8_pool_hw = (volatile int*)0x43CB0048;

//Fire 9 layer registers
volatile char *control_9 = (volatile char*)0x43CC0000;
volatile int *wg_x_9 = (volatile int*)0x43CC0010;
volatile int *wg_y_9 = (volatile int*)0x43CC0018;
volatile int *wg_z_9 = (volatile int*)0x43CC0020;
volatile int *fire9squeeze1x1_Weights_hw = (volatile int*)0x43CC0030;
volatile int *fire9expand1x1_Weights_hw = (volatile int*)0x43CC0040;
volatile int *fire9expand3x3_Weights_hw = (volatile int*)0x43CC0048;
volatile int *fire9squeeze1x1_Features_hw = (volatile int*)0x43CC0038;
volatile int *fire9_Features_hw = (volatile int*)0x43CC0050;
volatile int *Pool_Layer8_Features_hw = (volatile int*)0x43CC0028;

//Convolution layer 10 registers
volatile char *control_10 = (volatile char*)0x43CD0000;
volatile int *wg_x_10 = (volatile int*)0x43CD0010;
volatile int *wg_y_10 = (volatile int*)0x43CD0018;
volatile int *wg_z_10 = (volatile int*)0x43CD0020;
volatile int *layer_10_Weights_hw = (volatile int*)0x43CD0028;
volatile int *fire9_features_hw = (volatile int*)0x43CD0030;
volatile int *layer_10_features_hw = (volatile int*)0x43CD0038;

//Global pooling layer registers
volatile char *control_gp = (volatile char*)0x43CE0000;
volatile int *wg_x_gp = (volatile int*)0x43CE0010;
volatile int *wg_y_gp = (volatile int*)0x43CE0018;
volatile int *wg_z_gp = (volatile int*)0x43CE0020;
volatile int *o_x_gp = (volatile int*)0x43CE0028;
volatile int *o_y_gp = (volatile int*)0x43CE0030;
volatile int *o_z_gp = (volatile int*)0x43CE0038;
volatile int *Layer10_Features_hw = (volatile int*)0x43CE0040;
volatile int *output_hw = (volatile int*)0x43CE0048;



//Subroutine to execute the kernel on the Hardware
void executeKernel(volatile char *control,volatile int *group_id,int num_groups)
{
	print("Status of control register: \n\r");
	unsigned int con = *control;
	int i;
	for (i = 0; i < 8; i ++)
	{
	 	if (con & (1 << i))
	 	{
	 		print("1");
	 	}
	 	else
	 	{
	 		print("0");
	 	}
	}
	print("\n\r");
	print("Starting OpenCL kernel execution\n\r");
 	for (i = 0; i < num_groups; i ++) 
	{
	 	*group_id = i;
	 	*control |= 1; /* start */
	 	while (! ((*control) & 2)); /* waiting for hardware to report "done" */
	}
	print("\nDONE!\n\r");
}


int main() {
	init_platform(); /* more initialization */
	Xil_SetTlbAttributes(0x43C00000,0x10C06); /* non cacheable */

/* Declaration of pointers for the host side*/
	float *Layer1_Weights_CPU;
	float *Layer1_Features_CPU;
	int *Data_Layer_CPU;
	int *Data_Layer_CPU_R;
	int *Data_Layer_CPU_G;
	int *Data_Layer_CPU_B;
	float *Layer1_Pool_CPU;
	float *fire2squeeze1x1_Weights_CPU;
	float *fire2squeeze1x1_Features_CPU;
	float *fire2expand1x1_Weights_CPU;
	float *fire2_Features_CPU;
	float *fire2expand3x3_Weights_CPU;
	float *fire3expand3x3_Weights_CPU;
	float *fire3expand1x1_Weights_CPU;
	float *fire3squeeze1x1_Weights_CPU;
	float *fire3_Features_CPU;
	float *fire3squeeze1x1_Features_CPU;
	float *fire4expand3x3_Weights_CPU;
	float *fire4expand1x1_Weights_CPU;
	float *fire4squeeze1x1_Weights_CPU;
	float *fire4_Features_CPU;
	float *fire4squeeze1x1_Features_CPU;
	float *Layer4_Pool_CPU;
	float *fire5expand3x3_Weights_CPU;
	float *fire5expand1x1_Weights_CPU;
	float *fire5squeeze1x1_Weights_CPU;
	float *fire5_Features_CPU;
	float *fire5squeeze1x1_Features_CPU;
	float *fire6expand3x3_Weights_CPU;
	float *fire6expand1x1_Weights_CPU;
	float *fire6squeeze1x1_Weights_CPU;
	float *fire6_Features_CPU;
	float *fire6squeeze1x1_Features_CPU;
	float *fire7squeeze1x1_Weights_host;
	float *fire7expand1x1_Weights_host;
	float *fire7expand3x3_Weights_host;
	float* fire7squeeze1x1_Features_host;
	float* fire6_Features_host;
	float* fire7_Features_host;
	float *fire8squeeze1x1_Weights_host;
	float* fire8squeeze1x1_Features_host;
	float *fire8expand1x1_Weights_host;
	float *fire8expand3x3_Weights_host;
	float* fire8_Features_host;
	float* layer8_pool_host;
	float *fire9squeeze1x1_Weights_host;
	float *fire9expand1x1_Weights_host;
	float *fire9expand3x3_Weights_host;
	float* fire9squeeze1x1_Features_host;
	float* fire9_Features_host;
	float *layer_10_Weights_host;
	float *layer_10_features_host;
    float* output_host;



	/* Allocating host memory */
	/*Layer1 conv and pooling memory*/
	Layer1_Weights_CPU = (float*) malloc (3*96*49*NUM* sizeof(float));
	Data_Layer_CPU = (int*) malloc (3*227*227*NUM*sizeof(int));
	Data_Layer_CPU_R = (int*) malloc (227*227*NUM*sizeof(int));
	Data_Layer_CPU_G = (int*) malloc (227*227*NUM*sizeof(int));
	Data_Layer_CPU_B = (int*) malloc (227*227*NUM*sizeof(int));
	Layer1_Features_CPU = (float*) malloc (111*111*96* NUM * sizeof(float));
	Layer1_Pool_CPU = (float*) malloc (290400* NUM*sizeof(float));

	/* Fire2 layer memory */
	fire2squeeze1x1_Weights_CPU = (float*) malloc (1536* NUM*sizeof(float));
	fire2squeeze1x1_Features_CPU = (float*) malloc (55*55*16* NUM*sizeof(float));
	fire2expand1x1_Weights_CPU=(float*) malloc (1024* NUM*sizeof(float));
	fire2expand3x3_Weights_CPU=(float*) malloc (9216* NUM*sizeof(float));
	fire2_Features_CPU =(float*) malloc (55*55*128* NUM*sizeof(float));

	/* Fire3 layer memory */
	fire3squeeze1x1_Features_CPU=(float*) malloc (55*55*16* NUM*sizeof(float));
	fire3squeeze1x1_Weights_CPU=(float*) malloc (2048* NUM*sizeof(float));
    fire3expand1x1_Weights_CPU=(float*) malloc (1024* NUM*sizeof(float));
    fire3expand3x3_Weights_CPU=(float*) malloc (64*9*16* NUM*sizeof(float));
    fire3_Features_CPU=(float*) malloc (55*55*128* NUM*sizeof(float));

	/* Fire4 layer memory */
	fire4squeeze1x1_Features_CPU=(float*) malloc (55*55*32* NUM*sizeof(float));
	fire4squeeze1x1_Weights_CPU=(float*) malloc (4096* NUM*sizeof(float));
    fire4expand1x1_Weights_CPU=(float*) malloc (4096* NUM*sizeof(float));
    fire4expand3x3_Weights_CPU=(float*) malloc (36864* NUM*sizeof(float));
    fire4_Features_CPU=(float*) malloc (55*55*256* NUM*sizeof(float));
    Layer4_Pool_CPU = (float*) malloc (186624* NUM*sizeof(float));

    /* Fire5 layer memory */
	fire5squeeze1x1_Features_CPU=(float*) malloc (27*27*32* NUM*sizeof(float));
	fire5squeeze1x1_Weights_CPU=(float*) malloc (8192* NUM*sizeof(float));
    fire5expand1x1_Weights_CPU=(float*) malloc (4096* NUM*sizeof(float));
    fire5expand3x3_Weights_CPU=(float*) malloc (36864* NUM*sizeof(float));
    fire5_Features_CPU=(float*) malloc (27*27*256* NUM*sizeof(float));

	/* Fire6 layer memory */
	fire6squeeze1x1_Features_CPU=(float*) malloc (27*27*48* NUM*sizeof(float));
	fire6squeeze1x1_Weights_CPU=(float*) malloc (12288* NUM*sizeof(float));
    fire6expand1x1_Weights_CPU=(float*) malloc (9216* NUM*sizeof(float));
    fire6expand3x3_Weights_CPU=(float*) malloc (82944* NUM*sizeof(float));
    fire6_Features_CPU=(float*) malloc (27*27*384* NUM*sizeof(float));

	/* Fire7 layer memory */
	fire7squeeze1x1_Weights_host = (float*) malloc (18432 * sizeof(float));
	fire7expand1x1_Weights_host = (float*) malloc (9216 * sizeof(float));
	fire7expand3x3_Weights_host = (float*) malloc (82944 * sizeof(float));
	fire7squeeze1x1_Features_host = (float*) malloc (13*13*64 * sizeof(float));
	fire6_Features_host = (float*) malloc (27*27*384 * sizeof(float));
	fire7_Features_host = (float*) malloc (27*27*384 * sizeof(float));

	/* Fire8 layer memory */
	fire8squeeze1x1_Weights_host = (float*) malloc (27*27*64 * sizeof(float));
	fire8squeeze1x1_Features_host = (float*) malloc (27*27*64 * sizeof(float));
	fire8expand1x1_Weights_host = (float*) malloc (16384 * sizeof(float));
	fire8expand3x3_Weights_host = (float*) malloc (147456 * sizeof(float));
	fire8_Features_host = (float*) malloc (27*27*512 * sizeof(float));
	layer8_pool_host = (float*) malloc (86528 * sizeof(float));

	/* Fire9 layer memory */
	fire9squeeze1x1_Weights_host = (float*) malloc (32768 * sizeof(float));
	fire9expand1x1_Weights_host = (float*) malloc (16384 * sizeof(float));
	fire9expand3x3_Weights_host = (float*) malloc (147456 * sizeof(float));
	fire9squeeze1x1_Features_host = (float*) malloc (13*13*64 * sizeof(float));
	fire9_Features_host = (float*) malloc (13*13*512 * sizeof(float));

	/* Layer10 memory */
	layer_10_Weights_host = (float*) malloc (512000 * sizeof(float));
	layer_10_features_host = (float*) malloc (1000*15*15 * sizeof(float));
	output_host = (float*) malloc (1000 * sizeof(float));



    int i;
	/*Copy weights data of all layers*/
    for(i=0;i<3*96*49*NUM;i++)
    {
    	Layer1_Weights_CPU[i]=weights_data_conv1[i];
    }
    for(i=0;i<3*227*227*NUM;i++)
    {
		Data_Layer_CPU[i]=input_data[i];
    }

    for(i=0;i<1536*NUM;i++)
    {
    	fire2squeeze1x1_Weights_CPU[i]=weights_data_fire2s1x1[i];
    }

    for(i=0;i<1024*NUM;i++)
    {
    	fire2expand1x1_Weights_CPU[i]=weights_data_fire2e1x1[i];
    }

    for(i=0;i<9216*NUM;i++)
    {
		fire2expand3x3_Weights_CPU[i]=weights_data_fire2e3x3[i];
    }

    for(i=0;i<1024*NUM;i++)
    {
		fire3expand1x1_Weights_CPU[i]=weights_data_fire3e1x1[i];
    }
    for(i=0;i<64*9*16*NUM;i++)
    {
		fire3expand3x3_Weights_CPU[i]=weights_data_fire3e3x3[i];
    }
    for(i=0;i<2048*NUM;i++)
    {
      	fire3squeeze1x1_Weights_CPU[i]=weights_data_fire3s1x1[i];
    }

    for(i=0;i<4096*NUM;i++)
    {
		fire4expand1x1_Weights_CPU[i]=weights_data_fire4e1x1[i];
    }
    for(i=0;i<36864*NUM;i++)
    {
      	fire4expand3x3_Weights_CPU[i]=weights_data_fire4e3x3[i];
    }
    for(i=0;i<4096*NUM;i++)
    {
      	fire4squeeze1x1_Weights_CPU[i]=weights_data_fire4s1x1[i];
    }
    for(i=0;i<4096*NUM;i++)
    {
      	fire5expand1x1_Weights_CPU[i]=weights_data_fire5e1x1[i];
    }
    for(i=0;i<36864*NUM;i++)
    {
      	fire5expand3x3_Weights_CPU[i]=weights_data_fire5e3x3[i];
    }
    for(i=0;i<8192*NUM;i++)
    {
      	fire5squeeze1x1_Weights_CPU[i]=weights_data_fire5s1x1[i];
    }

    for(i=0;i<9216*NUM;i++)
    {
      	fire6expand1x1_Weights_CPU[i]=weights_data_fire6e1x1[i];
    }
    for(i=0;i<82944*NUM;i++)
    {
      	fire6expand3x3_Weights_CPU[i]=weights_data_fire6e3x3[i];
    }
    for(i=0;i<12288*NUM;i++)
    {
      	fire6squeeze1x1_Weights_CPU[i]=weights_data_fire6s1x1[i];
    }
    for(i=0; i<18432; i++)
	{
    	fire7squeeze1x1_Weights_host[i] = weights_data_fire7s1x1[i];
    }

    for(i=0; i<9216; i++)
    {
    	fire7expand1x1_Weights_host[i] = weights_data_fire7e1x1[i];
    }
    for(i=0; i<82944; i++)
    {
    	fire7expand3x3_Weights_host[i] = weights_data_fire7e3x3[i];
    }
    for(i=0; i<27*27*64; i++)
    {
    	fire8squeeze1x1_Weights_host[i] = weights_data_fire8s1x1[i];
    }
    for(i=0; i<16384; i++)
    {
    	fire8expand1x1_Weights_host[i] = weights_data_fire8e1x1[i];
    }
    for(i=0; i<147456; i++)
    {
    	fire8expand3x3_Weights_host[i] = weights_data_fire8e3x3[i];
    }
	for(i=0; i<32768; i++)
    {
    	fire9squeeze1x1_Weights_host[i] = weights_data_fire9s1x1[i];
    }
    for(i=0; i<16384; i++)
    {
        fire9expand1x1_Weights_host[i] = weights_data_fire9e1x1[i];
    }
    for(i=0; i<147456; i++)
    {
    	fire9expand3x3_Weights_host[i] = weights_data_fire9e3x3[i];
    }
    for(i=0; i<512000; i++)
    {
    	layer_10_Weights_host[i] = weights_data_fire10[i];
    }


	/*Divide the input data into R,G,B data */

	for(i=0; i<3*227*227*NUM; i+=3)
	{
		Data_Layer_CPU_R[i/3] = Data_Layer_CPU[i];
		Data_Layer_CPU_G[i/3] = Data_Layer_CPU[i+1];
		Data_Layer_CPU_B[i/3] = Data_Layer_CPU[i+2];
	}

	Xil_DCacheFlush();


	/* Program registers: Map CPU pointers to hardware registers */
	*Layer1_weights_HW = (int)Layer1_Weights_CPU;
	*data_R =(int)Data_Layer_CPU_R;
	*data_G =(int)Data_Layer_CPU_G;
	*data_B =(int)Data_Layer_CPU_B;
	*Layer1_Features_HW = (int)Layer1_Features_CPU;

	*Layer1_Pool_HW_p =(int)Layer1_Pool_CPU;
	*Layer1_Features_HW_p=(int)Layer1_Features_CPU;

	*Layer2_pool_GPU_HW = (int)Layer1_Pool_CPU;
	*fire2squeeze1x1_Weights_HW=(int)fire2squeeze1x1_Weights_CPU;
	*fire2squeeze1x1_Features_HW=(int)fire2squeeze1x1_Features_CPU;
	*fire2expand1x1_Weights_HW=(int)fire2expand1x1_Weights_CPU;
	*fire2_Features_HW=(int)fire2_Features_CPU;
	*fire2expand3x3_Weights_HW=(int)fire2expand3x3_Weights_CPU;

	*fire2_Features_HW_new = (int)fire2_Features_CPU;
	*fire3squeeze1x1_Weights_HW=(int)fire3squeeze1x1_Weights_CPU;
	*fire3squeeze1x1_Features_HW =(int)fire3squeeze1x1_Features_CPU;

	*fire3_squeeze_HW=(int)fire3squeeze1x1_Features_CPU;
	*fire3expand1x1_Weights_HW=(int)fire3expand1x1_Weights_CPU;
	*fire3_Features_HW=(int)fire3_Features_CPU;
	*fire3expand3x3_Weights_HW=(int)fire3expand3x3_Weights_CPU;

	*fire3_Features_HW_new=(int)fire3_Features_CPU;
	*fire4expand3x3_Weights_HW=(int)fire4expand3x3_Weights_CPU;
	*fire4expand1x1_Weights_HW=(int)fire4expand1x1_Weights_CPU;
	*fire4squeeze1x1_Weights_HW=(int)fire4squeeze1x1_Weights_CPU;
	*fire4_Features_HW=(int)fire4_Features_CPU;
	*fire4squeeze1x1_Features_HW =(int)fire4squeeze1x1_Features_CPU;

	*fire4_Features_HW_new = (int)fire4_Features_CPU;
	*Layer4_Pool_HW_p =(int)Layer4_Pool_CPU;

	*Layer4_Pool_HW_p_new =(int)Layer4_Pool_CPU;
	*fire5expand3x3_Weights_HW=(int)fire5expand3x3_Weights_CPU;
	*fire5expand1x1_Weights_HW=(int)fire5expand1x1_Weights_CPU;
	*fire5squeeze1x1_Weights_HW=(int)fire5squeeze1x1_Weights_CPU;
	*fire5_Features_HW=(int)fire5_Features_CPU;
	*fire5squeeze1x1_Features_HW =(int)fire5squeeze1x1_Features_CPU;

	*fire5_Features_HW_new=(int)fire5_Features_CPU;
	*fire6expand3x3_Weights_HW=(int)fire6expand3x3_Weights_CPU;
	*fire6expand1x1_Weights_HW=(int)fire6expand1x1_Weights_CPU;
	*fire6squeeze1x1_Weights_HW=(int)fire6squeeze1x1_Weights_CPU;
	*fire6_Features_HW=(int)fire6_Features_CPU;
	*fire6squeeze1x1_Features_HW =(int)fire6squeeze1x1_Features_CPU;

	*fire7squeeze1x1_Weights_hw = (int)fire7squeeze1x1_Weights_host;
	*fire7expand1x1_Weights_hw = (int)fire7expand1x1_Weights_host;
	*fire7expand3x3_Weights_hw = (int)fire7expand3x3_Weights_host;
	*fire6_Features_hw = (int)fire6_Features_CPU;
	*fire7_Features_hw = (int)fire7_Features_host;
	*Features_7sq1x1_hw = (int)fire7squeeze1x1_Features_host;

	*fire8squeeze1x1_Weights_hw = (int)fire8squeeze1x1_Weights_host;
	*fire7_Features_hw_8s = (int)fire7_Features_host;
	*Features_8sq1x1_hw = (int)fire8squeeze1x1_Features_host;

	*fire8expand1x1_Weights_hw = (int)fire8expand1x1_Weights_host;
	*fire8expand3x3_Weights_hw = (int)fire8expand3x3_Weights_host;
	*Features_8sq1x1_hw_8e = (int)fire8squeeze1x1_Features_host;
	*fire8_Features_hw = (int)fire8_Features_host;
	*layer8_pool_hw = (int)layer8_pool_host;

	*fire9squeeze1x1_Weights_hw = (int)fire9squeeze1x1_Weights_host;
	*fire9expand1x1_Weights_hw = (int)fire9expand1x1_Weights_host;
	*fire9expand3x3_Weights_hw = (int)fire9expand3x3_Weights_host;
	*Pool_Layer8_Features_hw = (int)layer8_pool_host;
	*fire9squeeze1x1_Features_hw = (int)fire9squeeze1x1_Features_host;
	*fire9_Features_hw = (int)fire9_Features_host;

	*layer_10_Weights_hw = (int)layer_10_Weights_host;
	*fire9_features_hw = (int)fire9_Features_host;
	*layer_10_features_hw = (int)layer_10_features_host;

	*Layer10_Features_hw = (int)layer_10_features_host;
	*output_hw = (int)output_host;


	/* Set the workgroup identity */
	*wg_y = 0;
	*wg_z = 0;
	*wg_x = 0;

	*wg_y_p = 0;
	*wg_z_p = 0;
	*wg_x_p = 0;

	*wg_y_f2 = 0;
	*wg_z_f2 = 0;
	*wg_x_f2 = 0;

	*wg_y_f3_s = 0;
	*wg_z_f3_s = 0;
	*wg_x_f3_s = 0;

	*wg_y_f3_e = 0;
	*wg_z_f3_e = 0;
	*wg_x_f3_e = 0;

	*wg_y_f4 = 0;
	*wg_z_f4 = 0;
	*wg_x_f4 = 0;

	*wg_y_f4_p4 = 0;
	*wg_z_f4_p4 = 0;
	*wg_x_f4_p4 = 0;

	*wg_y_f5 = 0;
	*wg_z_f5 = 0;
	*wg_x_f5 = 0;

	*wg_y_f6 = 0;
	*wg_z_f6 = 0;
	*wg_x_f6 = 0;

	*wg_y_7 = 0;
	*wg_z_7 = 0;
	*wg_x_7 = 0;

	*wg_y_8s = 0;
	*wg_z_8s = 0;
	*wg_x_8s = 0;

	*wg_y_8e = 0;
	*wg_z_8e = 0;
	*wg_x_8e = 0;

	*wg_y_9 = 0;
	*wg_z_9 = 0;
	*wg_x_9 = 0;

	*wg_y_10 = 0;
	*wg_z_10 = 0;
	*wg_x_10 = 0;

	*wg_y_gp = 0;
	*wg_z_gp = 0;
	*wg_x_gp = 0;


	/* Execute kernels layer by layer */
	printf("\n wait for the 'All layers completed' message. It is going to to take a long time.....");
    executeKernel(control,wg_x,111);
    executeKernel(control_p,wg_x_p,111);
    executeKernel(control_f2,wg_x_f2,55);
    executeKernel(control_f3_s,wg_x_f3_s,55);
 	executeKernel(control_f3_e,wg_x_f3_e,55);
	executeKernel(control_f4,wg_x_f4,55);
    executeKernel(control_f4_p4,wg_x_f4_p4,55);
    executeKernel(control_f5,wg_x_f5,27);
	executeKernel(control_f6,wg_x_f6,27);
	executeKernel(control_7,wg_x_7,27);
    executeKernel(control_8s,wg_x_8s,27);
    executeKernel(control_8e,wg_x_8e,27);
    executeKernel(control_9,wg_x_9,13);
    executeKernel(control_10,wg_x_10,15);
    executeKernel(control_gp,wg_x_gp,27);

	float max = 0;
	int predicted_class = 0;
	for(i=0; i<1000; i++)
	{
		if(output_host[i] > max)
		{
			max = output_host[i];
			predicted_class = i;
		}
	}
    printf("\n All layers completed");
	printf("\n\n Predicted class: %d", predicted_class);


 	Xil_DCacheInvalidate();
    cleanup_platform();

	return 0;
}

