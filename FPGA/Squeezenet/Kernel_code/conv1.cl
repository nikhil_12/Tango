
__kernel void __attribute__ ((reqd_work_group_size(111,1,1)))conv1(__global float *Layer1_Weights_CPU, __global int *Data_Layer_CPU_R, __global int *Data_Layer_CPU_G, __global int *Data_Layer_CPU_B,__global float *Layer1_Features)
{
	int tid = get_local_id(0);
	int gid = get_group_id(0);

	int x = tid*2 + 3;
	int y = gid*2 + 3;
	for(int f=0; f<96; f++)
	{
				float result = 0;
				for(int i = x-3; i<=x+3; i++)
				{
    					for(int j=y-3; j<=y+3; j++)
    					{
						int x_index = i-x+3;
						int y_index = j-y+3;
						int m = (y_index)+(x_index)*7;
         					if(i<0 || j<0)
						{
							result+= 0;
						}
         					else if(j>226 || i>226)
						{
							result+= 0;
						}
         					else
						{
							float temp = Data_Layer_CPU_R[(y_index-3) + x*227 + y + (x_index-3)*227]*Layer1_Weights_CPU[m+f*147] + Data_Layer_CPU_G[(y_index-3) + x*227 + y + (x_index-3)*227]*Layer1_Weights_CPU[m+49+f*147] + Data_Layer_CPU_B[(y_index-3) + x*227 + y + (x_index-3)*227]*Layer1_Weights_CPU[m+98+f*147];
							result+= temp;
						}
					}
				}
				if(result < 0)
				{
					result = 0;
				}
				Layer1_Features[f*111*111+((x-3)/2)*111+((y-3)/2)] = result;
	}
}
